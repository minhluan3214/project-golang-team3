package auth

import (
	"../../services"
	"github.com/gin-gonic/gin"
)

func RouterAuth(g *gin.RouterGroup) *gin.RouterGroup {
	public := g.Group("authentications")

	public.POST("credentials", services.Credential)
	return public
}