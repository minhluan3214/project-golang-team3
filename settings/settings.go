package settings

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Database struct {
	User     string `yaml:"User"`
	Password string `yaml:"Password"`
	Database string `yaml:"Database"`
}

var DatabaseSetting = &Database{}

type App struct {
	PrivateKeyPath        string `yaml:"PrivateKeyPath"`
	PublicKeyPath         string `yaml:"PublicKeyPath"`
	PrivateKeyRefreshPath string `yaml:"PrivateKeyRefreshPath"`
	PublicKeyRefreshPath  string `yaml:"PublicKeyRefreshPath"`
	Host                  string `yaml:"Host"`
	CryptoKey             string `yaml:"CryptoKey"`
}

var AppSetting = &App{}

type Configuration struct {
	Database Database `yaml:"database"`
	App      App      `yaml:"app"`
}

func Setup() {
	yamlFile, err := ioutil.ReadFile("configs/default_config.yaml")
	if err != nil {
		fmt.Println(err)
	}
	conf := Configuration{}
	err = yaml.Unmarshal(yamlFile, &conf)
	if err != nil {
		fmt.Println(err)
	}
	DatabaseSetting = &conf.Database
	AppSetting = &conf.App
	if err != nil {
		fmt.Println("Error:", err)
	}
}
