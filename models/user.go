package models

import (
	"../pkg/commons/crypto"
	"../pkg/commons/email"
	"../pkg/requests"
	"../pkg/requests/accounts"
	"../pkg/responses"
	"../pkg/responses/messages"
	"../settings"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
	"time"
)

type User struct {
	ID                int       `gorm:"AUTO_INCREMENT;primary_key;column:id" json:"id"`
	Email             string     `gorm:"unique_index;not null; column:email" json:"email"`
	UserName          string     `gorm:"unique_index;not null; column:username" json:"userName"`
	FullName          string     `gorm:"column:fullname" json:"fullName"`
	Password          string     `gorm:"column:password" json:"password"`
	CountLock         int        `gorm:"column:countlock" json:"countLock"`
	SpendingTime      *time.Time `gorm:"column:spendingtime"`
	ForgotPwdCode     string     `gorm:"column:forgotpwdcode" json:"forgotPwdCode"`
	ExpiredForgotCode *time.Time `gorm:"column:expiredforgotcode" json:"expiredForgotCode"`
}

var dbName = GetTablePrefix("user")

func IsColumnDataExist(columnName string, columnData string) (bool, error) {
	userPortals := User{}
	//err := db.First(&user).Error
	err := db.Where(columnName+"=?", columnData).First(&userPortals).Error
	// db.Raw("SELECT ID, UserName, PasswordHash FROM OPortal.dbo.UserPortals Where UserName = 'user14'").Scan(&user)
	// log.Println("err: ", err)

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, nil
		}
		panic(err)
	}
	return true, nil
}

func IsEmailExist(email string) (bool, error) {
	return IsColumnDataExist("email", email)
}

func IsUsernameExist(username string) (bool, error) {
	return IsColumnDataExist("username", username)
}

func GetUser(user *User) *User {
	users := &User{}
	err := db.Where(user).First(&users).Error

	if err != nil {
		fmt.Println(err)
		return nil
	}
	return users
}

func FindUserById(id int) (*User, bool) {
	users := &User{}
	err := db.Where(&User{ID: id}).First(&users).Error

	if err != nil {
		fmt.Println(err)
		if err == gorm.ErrRecordNotFound {
			return users, false
		}
		panic(err)
	}
	return users, true
}

func FindUserByUserName(username string) (*User, bool) {
	users := &User{}
	err := db.Where(&User{UserName: username}).First(&users).Error

	if err != nil {
		fmt.Println(err)
		if err == gorm.ErrRecordNotFound {
			return users, false
		}
		panic(err)
	}
	return users, true
}


func FindUserByEmail(email string) (*User, bool) {
	users := &User{}
	err := db.Where(&User{Email: email}).First(&users).Error

	if err != nil {
		fmt.Println(err)
		if err == gorm.ErrRecordNotFound {
			return users, false
		}
		panic(err)
	}
	return users, true
}

func GetUsers(req requests.QueryArgs) (responses.ResultPaging, error) {
	res := responses.ResultPaging{}
	count :=0
	tblName :=GetTablePrefix("users")
	err := db.Table(tblName).Where("email LIKE ? OR fullname LIKE ?","%"+req.Filter+"%" ,"%"+req.Filter+"%" ).Count(&count).Error
	if count == 0{
		return res, err
	}
	res.TotalItems= count

	users := make([]*User, 0)
	err = db.Table(tblName).Where("email LIKE ? OR fullname LIKE ?","%"+req.Filter+"%" ,"%"+req.Filter+"%"  ).Find(&users).Error
	if err != nil {
		return res , err
	}
	res.Items = users
	return res, nil
}

func UpdatePwd(id int, newPwd string) responses.Response {
	user, exist:= FindUserById(id)
	if !exist{
		return responses.Response{
			Error:messages.GetMessage(messages.INVALID_PARAMS),
			StatusCode:http.StatusBadRequest,
		}
	}
	pwd, err := crypto.HashPassword(newPwd)
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.SYSTEM_ERROR),
			StatusCode:http.StatusInternalServerError,
		}
	}
	user.Password = pwd
	db.Save(&user)
	return responses.Response{
		StatusCode:http.StatusOK,
		Message:messages.GetMessage(messages.SUCCESS),
	}
}


func Create(user User) bool {
	added:= db.NewRecord(user)
	if added {
		db.Create(&user)
		return true
	}
	return false
}

func UpdateCountLock(user *User) (bool, error) {

	if(user.CountLock >10){
		err := CheckAndUpdatePendingTime(user)
		if err != nil{
			return false,err
		}
	}
	count:= user.CountLock + 1
	err:= db.Model(&user).Update("countlock", count).Error
	if err != nil {
		return false, err
	}
	return true, nil
}


func ClearCountLock(user *User)  error {
	err := db.Model(&user).Update("countlock", 0).Error
	if err != nil{
		return  err
	}
	return nil
}

func CheckAndUpdatePendingTime(user *User) error {
	now := time.Now()
	if user.SpendingTime != nil && user.SpendingTime.Before(now) {
		user.SpendingTime = nil
		user.CountLock = 0
		db.Save(&user)
	}
	if user.CountLock > 10 && user.SpendingTime == nil{
		now = now.Add(3*time.Minute)
		err:= db.Model(&user).Update("spendingtime", now).Error
		if err != nil {
			return err
		}
	}
	return nil
}

func UpdateForgotCode(user *User) error {
	now := time.Now().Add(4*time.Hour)
	token:= crypto.Encrypt(strconv.Itoa(user.ID), settings.AppSetting.CryptoKey)
	user.ForgotPwdCode = token
	db.Save(&user)
	err:= db.Model(&user).Update("expiredforgotcode", now).Error
	if err!= nil {
		return  err
	}
	link:= fmt.Sprintf("%s/api/v1/accounts/verify-forgotpwd/%s",settings.AppSetting.Host,token)
	to := []string{user.Email}
	strTemplate := "To: %s\r\n" +
		"Subject: discount Gophers!\r\n" +
		"\r\n" +
		"%s .\r\n"
	template:= fmt.Sprintf(strTemplate, user.Email, link)
	msg := []byte(template)
	email := email.EmailTemplate{
		Msg: msg,
		Recipients: to,
	}
	err = SendMail(email)
	return nil
}

func VerifyForgotPwdCode(token string) responses.Response  {
	now:= time.Now()
	var err error
	data, err := crypto.Decrypt(token, settings.AppSetting.CryptoKey)
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.DATA_NOTFOUND),
			StatusCode:http.StatusForbidden,
		}
	}
	fmt.Printf("Decrypted: %s\n", data)
	id, err := fmt.Printf("%s\n",data)
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.SYSTEM_ERROR),
			StatusCode:http.StatusInternalServerError,
		}
	}
	user, exist := FindUserById(id)
	if !exist{
		return responses.Response{
			Error:messages.GetMessage(messages.INVALID_PARAMS),
			StatusCode:http.StatusBadRequest,
		}
	}
	if user.ExpiredForgotCode.Before(now){
		return responses.Response{
			Error:messages.GetMessage(messages.TOKEN_EXPIRED),
			StatusCode:http.StatusBadRequest,
		}
	}
	if user.ForgotPwdCode != token{
		return responses.Response{
			Error:messages.GetMessage(messages.TOKEN_EXPIRED),
			StatusCode:http.StatusBadRequest,
		}
	}
	return responses.Response{
		Message:messages.GetMessage(messages.TOKEN_AVAILABLE),
		StatusCode:http.StatusOK,
	}
}

func UpdateForgotPwd(req accounts.UpdateForgotPwd) responses.Response {
	data, err := crypto.Decrypt(req.Token, settings.AppSetting.CryptoKey)
	res := VerifyForgotPwdCode(req.Token)
	if res.StatusCode != http.StatusOK{
		return res
	}
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.DATA_NOTFOUND),
			StatusCode:http.StatusForbidden,
		}
	}
	fmt.Printf("Decrypted: %s\n", data)
	id, err := fmt.Printf("%s\n",data)
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.SYSTEM_ERROR),
			StatusCode:http.StatusInternalServerError,
		}
	}
	user , exist:= FindUserById(id)
	if !exist{
		return responses.Response{
			Error:messages.GetMessage(messages.INVALID_PARAMS),
			StatusCode:http.StatusBadRequest,
		}
	}
	pwd, err := crypto.HashPassword(req.Password)
	if err != nil{
		return responses.Response{
			Error:messages.GetMessage(messages.SYSTEM_ERROR),
			StatusCode:http.StatusInternalServerError,
		}
	}
	user.Password = pwd
	user.ForgotPwdCode = ""
	err = db.Save(&user).Error
	if err != nil {
		return responses.Response{
			Error:messages.GetMessage(messages.SYSTEM_ERROR),
			StatusCode:http.StatusInternalServerError,
		}
	}
	return responses.Response{
		Message:messages.GetMessage(messages.SUCCESS),
		StatusCode:http.StatusOK,
	}
}