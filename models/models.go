package models

import (
	"fmt"
	"log"
	"net/smtp"

	"../pkg/commons/email"
	"../settings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func Setup() {
	connectionString := fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", settings.DatabaseSetting.User, settings.DatabaseSetting.Password, settings.DatabaseSetting.Database)
	var err error
	db, err = gorm.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}
	if err != nil {
		log.Panic(err)
	}
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return settings.DatabaseSetting.Database +"."+ defaultTableName
	}
	if err != nil {
		log.Panic(err)
	}
	if err = db.Error; err != nil {
		log.Panic(err)
	}
	fmt.Println("DB Connected")
}

func GetTablePrefix(tblName string) string {
	return fmt.Sprintf("%s.%s", settings.DatabaseSetting.Database, tblName)
}

func SendMail(email email.EmailTemplate) error {
	hostname := "smtp.gmail.com"
	auth := smtp.PlainAuth("", "appinfo.emailtest@gmail.com", "P@ssword123@", hostname)
	//to := []string{"recipient@example.net"}
	//msg := []byte("To: recipient@example.net\r\n" +
	//	"Subject: discount Gophers!\r\n" +
	//	"\r\n" +
	//	"This is the email body.\r\n")
	err := smtp.SendMail(hostname+":587", auth, "sender@example.org", email.Recipients, email.Msg)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}