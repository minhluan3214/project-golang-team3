package main

import (
	"os"

	"./controllers"
	"./core/utils"
	"./docs"
	"./models"
	"./settings"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func init() {
	settings.Setup()
	models.Setup()
	utils.Setup()
}

// @Security bearer
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8801" //localhost
	}

	docs.SwaggerInfo.Title = "Project Team 3"
	docs.SwaggerInfo.Description = "Authentication mockup project"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.BasePath = "/api/v1"
	routers := controllers.InitRouter()
	routers.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	routers.Run(":" + port)

}
