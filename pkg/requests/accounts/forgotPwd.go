package accounts

type ForgotPwd struct {
	Email string `json:"email"`
}

type UpdateForgotPwd struct {
	Password string `json:"password"`
	Token string `json:"token"`
}