package messages

var MessageFlags = map[int]string{
	SUCCESS:            "OK",
	ERROR:              "Fail",
	SYSTEM_ERROR:       "System error",
	INVALID_PARAMS:     "Bad request",
	DATA_NOTFOUND:      "Data not found",
	LOGIN_MESSAGE:      "Username or password dose not match",
	WRONG_CREDENTIALS:  "Information isn't correct",
	LOG_OUT_ENABLED:    "User is not valid or locked",
	NOT_ALLOWED_LOGIN:  "User is not allowed to login",
	BAD_GATEWAY:        "Bad gateway",
	CREATE_SUCCESS:     "Created Successfully",
	SPENDING_LOGIN:     "Spending 3 Minutes",
	DUPLICATE_USER:     "Email or Username is Exist",
	TOKEN_EXPIRED:      "Token Expired",
	TOKEN_AVAILABLE:    "Token Available",
	FORMAT_EMAIL_ERROR: "Email is malformed ",
}

func GetMessage(code int) string {
	message, ok := MessageFlags[code]
	if ok {
		return message
	}
	return MessageFlags[ERROR]
}
