package email

type EmailTemplate struct {
	Recipients []string
	Msg []byte
}